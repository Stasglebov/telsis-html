import { TweenLite } from 'gsap';

export default class Figure1 {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      blocksColor: '#e72064',
      backgroundColor: 'rgba(0,0,0,0)',
      isAnimated: true
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    let _that = this;
    this.rectWidth = {
      value: 60
    }
    this.rectHeight = {
      value: 35
    }
    this.rectsQuantity = 7
    this.rectsSkewX = {
      value: 0.65
    }
    this.rectsSkewY = {
      value: 25
    }
    this.startAlpha = {
      value: 1
    }
    this.alphaAnimation = {
      value: 1
    }
    // this.tlStart = new TimelineMax();
    this.tl = new TimelineMax({repeat: 3, yoyo: true, delay: 1, repeatDelay: 1})
    this.rectsOffset = {
      value: 9
    }
    this.elemsOffset = [
      {
        value: 0 // not showing
      },
      {
        value: -30
      },
      {
        value: -5
      },
      {
        value: 8
      },
      {
        value: 15
      },
      {
        value: 25
      },
      {
        value: 35
      }
    ]
    // this.elemsOffset = [
    //   {
    //     value: 0 // not showing
    //   },
    //   {
    //     value: -35
    //   },
    //   {
    //     value: -35
    //   },
    //   {
    //     value: -25
    //   },
    //   {
    //     value: -10
    //   },
    //   {
    //     value: 10
    //   },
    //   {
    //     value: 35
    //   }
    // ]
    this.moveY = {
      value: 0
    }
    this.moveYSec = {
      value: 0
    }
    this.fillOpacity = {
      value: 1
    }
    this.fillColorStyle = '#e72064'
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    
    this.render();
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    _that.ctx.restore()
  
  
    if ( window.innerWidth > 1200 || window.innerWidth < 767 ) {
  
      _that.tl
        .to(_that.elemsOffset[1], 1, {
          value: -35,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.elemsOffset[2], 1, {
          value: -35,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[3], 1, {
          value: -25,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[4], 1, {
          value: -10,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[5], 1, {
          value: 10,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[6], 1, {
          value: 35,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        // to begin
        .to(_that.elemsOffset[1], 1, {
          value: -30,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.elemsOffset[2], 1, {
          value: -5,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[3], 1, {
          value: 8,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[4], 1, {
          value: 15,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[5], 1, {
          value: 25,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.elemsOffset[6], 1, {
          value: 35,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
    }
    _that.drawRects()
    
  }
  
  drawRects () {
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    for (let i = 0; i < _that.rectsQuantity; i++) {
      
      _that.ctx.save()
      _that.ctx.beginPath()
      let getAlpha = function() {
        if ( _that.startAlpha.value - _that.alphaAnimation.value + i/5 > 0) {
          return _that.startAlpha.value - _that.alphaAnimation.value + i/5;
        } else {
          return 0;
        }
      }
      _that.ctx.globalAlpha = getAlpha();
      
      _that.ctx.moveTo(_that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value - _that.moveYSec.value + _that.elemsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2, _that.canvas.height / 2 - _that.rectHeight.value / 2 + 12 - _that.moveY.value - _that.moveYSec.value + _that.elemsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2 - _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 + _that.rectHeight.value / 2 - _that.moveY.value - _that.moveYSec.value + _that.elemsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2, _that.canvas.height / 2 + _that.rectHeight.value / 2 - 12 - _that.moveY.value - _that.moveYSec.value + _that.elemsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2 + _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value - _that.moveYSec.value + _that.elemsOffset[i].value)
      _that.ctx.fillStyle = `rgba(231,32,100,${_that.fillOpacity.value})`;
      _that.ctx.fill();
      _that.ctx.closePath()
      // _that.ctx.fillStyle = _that.blocksColor;
      // _that.ctx.fill()
      _that.ctx.restore()
    }
  }
  
  onResize () {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
      // window.location.reload()
    })
  }
}
