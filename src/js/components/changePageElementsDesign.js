import {
  currentPage
} from '../modules/dev/_helpers';

window.addEventListener('load', () => {
  if ( currentPage === 'solutions') {
    document.querySelector('.scroll-up').remove();
  }
})