import AOS from 'aos';
export default class Faded {
  static init() {
    window.addEventListener('load', () => {
      let offsetValue = 200;
      if ( window.innerWidth < 1600 ) {
        offsetValue = 50
      }
      AOS.init({
        offset: offsetValue,
        duration: 600,
        disable: window.innerWidth < 1200
      });
    });
  }
}