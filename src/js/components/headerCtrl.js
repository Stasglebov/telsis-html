import {
  throttle,
  Resp
} from '../modules/dev/_helpers'
export default class ScrollHeaderToggle {
  static initThrottle() {
    // let initedWithThrottle = throttle(this.toggle, 50);
    // window.addEventListener('scroll', initedWithThrottle, true);
    
    // this.toggle();
  }
  static toggle() {
    let header = document.querySelector('.header'),
      headerHeight = header.clientHeight,
      hideHeight = Resp.isMobile ? 100 : 350,
      nav = document.querySelector('.nav');
    
  
    let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
    
    if ( nav.classList.contains('nav-open') ) return
    
    if (scrollTop > this.lastScrollTop && scrollTop > headerHeight) {
      header.classList.add('header_hide');
    }
    else if (scrollTop + window.innerHeight < document.documentElement.scrollHeight) {
      header.classList.remove('header_hide');
    }
   
    this.lastScrollTop = scrollTop;
  }
}
