import { TweenLite } from 'gsap';
import {
  getScrollBarWidth,
  currentPage
} from '../modules/dev/_helpers';

export default class AboutShapes {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      blocksColor: '#e72064',
      backgroundColor: 'rgba(0,0,0,0)'
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    this.shapesTopQuantity = 4;
    this.shapesBottomQuantity = 4;
    this.onResize()
    this.init()
    
  }
  
  init () {
    this.offsetTop = {
      value: 25
    }
    this.offsetBottom = {
      value: 25
    }
    
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    this.tl = new TimelineMax({repeat: -1, yoyo: true})
    this.ctx.fillStyle = 'rgba(0,0,0,0)'
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)
    this.render()
    
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    
    _that.tl
      .to(_that.offsetTop, 4, {
        value: 50,
        ease: Elastic.easeOut.config(1, 0.3),
        onUpdate: function() {
          _that.drawRects();
        }
      })
      .to(_that.offsetBottom, 4, {
        value: 80,
        ease: Elastic.easeOut.config(1, 0.3),
        onUpdate: function() {
          _that.drawRects();
        }
      }, '-=4')
    _that.drawRects();
  }
  
  drawRects () {
    
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
  
      // draw top shape shadow
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.moveTo(_that.canvas.width * 0.4375, 360 + 60 );
      _that.ctx.lineTo(_that.canvas.width * 0.918, 0 + 60);
      _that.ctx.lineTo(_that.canvas.width * 1.15, 190 + 60);
      _that.ctx.lineTo(_that.canvas.width * 0.68, 570 + 60);
      _that.ctx.closePath();
      _that.ctx.fillStyle = 'rgba(0,0,0,0.1)';
      _that.ctx.filter = 'blur(15px)'
      _that.ctx.fill();
      _that.ctx.restore();
      // draw top shape
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.moveTo(_that.canvas.width * 0.4375, 360 );
      _that.ctx.lineTo(_that.canvas.width * 0.918, 0);
      _that.ctx.lineTo(_that.canvas.width * 1.15, 190);
      _that.ctx.lineTo(_that.canvas.width * 0.68, 570);
      _that.ctx.closePath();
      _that.ctx.globalAlpha = '0.7';
      _that.ctx.fillStyle = '#fff';
      _that.ctx.fill();
      _that.ctx.restore();
      // draw ellipse top
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.ellipse(_that.canvas.width * 0.62, 390, 17, 11, 0, 0, 2*Math.PI);
      _that.ctx.closePath();
      _that.ctx.lineWidth = 2;
      _that.ctx.strokeStyle = '#fff';
      _that.ctx.stroke();
      _that.ctx.restore();
      // draw bottom shape shadow
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.moveTo(_that.canvas.width * 0.32, 460 + 60);
      _that.ctx.lineTo(_that.canvas.width * 0.560, 670 + 60);
      _that.ctx.lineTo(_that.canvas.width * 0.08, 1050 + 60);
      _that.ctx.lineTo(_that.canvas.width * -0.15, 800 + 60);
      _that.ctx.closePath();
      _that.ctx.fillStyle = 'rgba(0,0,0,0.1)';
      _that.ctx.filter = 'blur(15px)'
      _that.ctx.fill();
      _that.ctx.restore();
      for ( let i = 0; i < _that.shapesBottomQuantity; i++) {
        _that.ctx.save();
        _that.ctx.beginPath();
        _that.ctx.moveTo(_that.canvas.width * 0.08, 750 + _that.offsetBottom.value*i );
        _that.ctx.lineTo(_that.canvas.width * 0.215, 650 + _that.offsetBottom.value*i);
        _that.ctx.lineTo(_that.canvas.width * 0.307, 726 + _that.offsetBottom.value*i);
        _that.ctx.lineTo(_that.canvas.width * 0.165, 840 + _that.offsetBottom.value*i);
        _that.ctx.closePath();
        _that.ctx.globalAlpha = `${i * 0.2 + 0.2}`;
        _that.ctx.fillStyle = '#e864a6';
        _that.ctx.fill();
        _that.ctx.restore();
      }
      // draw bottom shape
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.moveTo(_that.canvas.width * 0.32, 460 );
      _that.ctx.lineTo(_that.canvas.width * 0.560, 670);
      _that.ctx.lineTo(_that.canvas.width * 0.08, 1050);
      _that.ctx.lineTo(_that.canvas.width * -0.15, 800);
      _that.ctx.closePath();
      _that.ctx.globalAlpha = '0.7';
      _that.ctx.fillStyle = '#fff';
      _that.ctx.fill();
      _that.ctx.restore();
      // draw ellipse bottom
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.ellipse(_that.canvas.width * 0.365, 595, 17, 11, 0, 0, 2*Math.PI);
      _that.ctx.closePath();
      _that.ctx.lineWidth = 2;
      _that.ctx.strokeStyle = '#fff';
      _that.ctx.stroke();
      _that.ctx.restore();
      // draw line connect
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.moveTo(_that.canvas.width * 0.365 + 12, 595 - 8)
      _that.ctx.lineTo(_that.canvas.width * 0.62 - 12, 390 + 8)
      _that.ctx.closePath();
      _that.ctx.lineWidth = 2;
      _that.ctx.strokeStyle = '#fff';
      _that.ctx.stroke();
      _that.ctx.restore();
      
      // draw shape top 1
      for ( let i = 0; i < _that.shapesTopQuantity; i++) {
        _that.ctx.save();
        _that.ctx.beginPath();
        _that.ctx.moveTo(_that.canvas.width * 0.65, 303 - _that.offsetTop.value*i );
        _that.ctx.lineTo(_that.canvas.width * 0.824, 175 - _that.offsetTop.value*i);
        _that.ctx.lineTo(_that.canvas.width * 0.9, 240 - _that.offsetTop.value*i);
        _that.ctx.lineTo(_that.canvas.width * 0.73, 370 - _that.offsetTop.value*i);
        _that.ctx.closePath();
        _that.ctx.globalAlpha = `${i * 0.2 + 0.2}`;
        _that.ctx.fillStyle = '#e864a6';
        _that.ctx.fill();
        _that.ctx.restore();
      }
  
    
    
      
  }
  onResize () {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
      // window.location.reload()
    })
  }
  
}
