export default class Dropdown {
  constructor () {
    this.input = document.getElementById('product')
    this.dropdownText = document.querySelector('.dropdown_item__text')
    this.dropdownItem = document.querySelector('.dropdown_item')
    this.dropdownProduct = document.querySelector('.dropdown_product')
    this.productsList = [...document.querySelectorAll('.dropdown_product__product')]
    this.dropdownArrow = document.querySelector('.dropdown_item__arrow')
    this.init()
  }
  
  init () {
    this.initShowListEvent()
    this.initTextChangeEvent()
  }
  
  initShowListEvent () {
    document.addEventListener('click', (e) => {
      
      if (e.target.classList.contains('dropdown_item__text') || e.target.classList.contains('dropdown_item') || e.target.classList.contains('arrow_img')) {
        e.preventDefault()
        this.dropdownProduct.classList.toggle('dropdown_product__active')
        this.dropdownArrow.classList.toggle('dropdown_item__arrow-active')
      } else {
        this.dropdownProduct.classList.remove('dropdown_product__active')
        this.dropdownArrow.classList.remove('dropdown_item__arrow-active')
      }
    })
  };
  
  initTextChangeEvent () {
    let self = this;
    console.log(this.productsList);
    for (let i = 0; i < this.productsList.length; i++) {
      this.productsList[i].addEventListener('click', function () {
        let text = this.textContent;
        self.dropdownText.textContent = text;
        self.input.value = text;
      });
    }
    
  }
  
};