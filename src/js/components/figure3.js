import { TweenLite } from 'gsap';

export default class Figure1 {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      blocksColor: '#e72064',
      backgroundColor: 'rgba(0,0,0,0)',
      isAnimated: true
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    let _that = this;
    this.rectWidth = {
      value: 60
    }
    this.rectHeight = {
      value: 35
    }
    this.rectsQuantity = 7
    this.rectsSkewX = {
      value: 0.65
    }
    this.rectsSkewY = {
      value: 25
    }
    this.startAlpha = {
      value: 1
    }
    this.alphaAnimation = {
      value: 1
    }
    // this.tlStart = new TimelineMax();
    this.tl = new TimelineMax({repeat: 3, repeatDelay: 1, delay: 1})
    this.rectsOffset = {
      value: 9
    }
    this.moveY = {
      value: 0
    }
    this.moveYSec = {
      value: -38
    }
    this.fillOpacity = {
      value: 1
    }
    this.yOff1 = {
      value: 0
    }
    this.xOff1 = {
      value: 0
    }
    this.xOff2 = {
      value: 0
    }
    this.yOff2 = {
      value: 0
    }
    this.opacity1 = {
      value: 1
    }
    this.opacity2 = {
      value: 0.5
    }
    this.opacity3 = {
      value: 0.4
    }
    this.opacity4 = {
      value: 0.5
    }
    this.fillColorStyle = '#e72064'
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    
    this.render();
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    _that.ctx.restore()
  
  
    if ( window.innerWidth > 1200 || window.innerWidth < 767 ) {
  
      _that.tl
        .to(_that.yOff1, 0.3, {
          value: -7,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.opacity1, 0.3, {
          value: 0.7,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        .to(_that.xOff1, 0.3, {
          value: 7,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.15')
        .to(_that.opacity2, 0.3, {
          value: 0.3,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        .to(_that.yOff2, 0.3, {
          value: 7,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.15')
        .to(_that.opacity3, 0.3, {
          value: 0.2,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        .to(_that.xOff2, 0.3, {
          value: -7,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.15')
        .to(_that.opacity4, 0.3, {
          value: 0.4,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        // to begin
        .to(_that.yOff1, 0.3, {
          value: 0,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.opacity1, 0.3, {
          value: 1,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        .to(_that.xOff1, 0.3, {
          value: 0,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.15')
        .to(_that.opacity2, 0.3, {
          value: 0.5,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        .to(_that.yOff2, 0.3, {
          value: 0,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.15')
        .to(_that.opacity3, 0.3, {
          value: 0.4,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        .to(_that.xOff2, 0.3, {
          value: 0,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.15')
        .to(_that.opacity4, 0.3, {
          value: 0.5,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
  
    }
    _that.drawRects()
    
  }
  
  drawRects () {
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    
    
      // top
      _that.ctx.save()
      _that.ctx.globalAlpha = _that.opacity1.value;
      _that.ctx.beginPath()
      _that.ctx.moveTo(47, 7 + _that.yOff1.value)
      _that.ctx.lineTo(67, 19 + _that.yOff1.value)
      _that.ctx.lineTo(27,42 + _that.yOff1.value)
      _that.ctx.lineTo(7, 30 + _that.yOff1.value)
      _that.ctx.lineTo(47, 7 + _that.yOff1.value)
      _that.ctx.fillStyle = `rgba(231,32,100,${_that.fillOpacity.value})`;
      _that.ctx.fill();
      _that.ctx.closePath()
      _that.ctx.restore()
      // right
      _that.ctx.save()
      _that.ctx.globalAlpha = _that.opacity2.value;
      _that.ctx.beginPath()
      _that.ctx.moveTo(48 + _that.xOff1.value, 7)
      _that.ctx.lineTo(67 + _that.xOff1.value, 19)
      _that.ctx.lineTo(67 + _that.xOff1.value,77)
      _that.ctx.lineTo(48 + _that.xOff1.value, 64)
      _that.ctx.lineTo(48 + _that.xOff1.value, 7)
      _that.ctx.fillStyle = `rgba(231,32,100,${_that.fillOpacity.value})`;
      _that.ctx.fill();
      _that.ctx.closePath()
      _that.ctx.restore()
      // bottom
      _that.ctx.save()
      _that.ctx.globalAlpha = _that.opacity3.value;
      _that.ctx.beginPath()
      _that.ctx.moveTo(48, 64 + _that.yOff2.value)
      _that.ctx.lineTo(67, 77 + _that.yOff2.value)
      _that.ctx.lineTo(27, 99 + _that.yOff2.value)
      _that.ctx.lineTo(7, 87 + _that.yOff2.value)
      _that.ctx.lineTo(48, 64 + _that.yOff2.value)
      _that.ctx.fillStyle = `rgba(231,32,100,${_that.fillOpacity.value})`;
      _that.ctx.fill();
      _that.ctx.closePath()
      _that.ctx.restore()
      // left
      _that.ctx.save()
      _that.ctx.globalAlpha = _that.opacity4.value;
      _that.ctx.beginPath()
      _that.ctx.moveTo(27 + _that.xOff2.value, 99)
      _that.ctx.lineTo(7 + _that.xOff2.value, 87)
      _that.ctx.lineTo(7 + _that.xOff2.value, 30)
      _that.ctx.lineTo(27 + _that.xOff2.value, 42)
      _that.ctx.lineTo(27 + _that.xOff2.value, 99)
      _that.ctx.fillStyle = `rgba(231,32,100,${_that.fillOpacity.value})`;
      _that.ctx.fill();
      _that.ctx.closePath()
      _that.ctx.restore()
  }
  
  onResize () {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
      // window.location.reload()
    })
  }
}
