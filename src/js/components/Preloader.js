import { TweenLite } from 'gsap';

export default class Preloader {
  
  constructor (containerClass, options) {

    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }

          let to = Object(target)

          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]

            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
  
    // set default options
    const defaults = {
      blocksColor: '#e72064',
      backgroundColor: '#171617',
      isAnimated: true
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    let _that = this;
    this.rectWidth = {
      value: 130
    }
    this.rectHeight = {
      value: 75
    }
    this.rectsQuantity = 4
    this.rectsSkewX = {
      value: 0.65
    }
    this.rectsSkewY = {
      value: 25
    }
    // this.tlStart = new TimelineMax();
    this.tl = new TimelineMax({repeat: -1, repeatDelay: 0})
    this.rectsOffset = {
      value: 0
    }
    this.moveY = {
      value: 0
    }
    this.moveYSec = {
      value: 0
    }
    this.startAlpha = {
      value: 0
    }
    this.alphaAnimation = {
      value: 1
    }
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    this.render();
    document.addEventListener('load', this.contentLoaded())
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  contentLoaded() {
    setTimeout(() => {
      this.container.classList.add('loaded')
      document.querySelector('.intro .right').classList.add('animationStart')
      document.querySelector('.intro .scroll').classList.add('animationStart')
    }, 2500)
    
  }
  
  
  
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.blocksColor;
    _that.ctx.restore()
    if ( _that.isAnimated ) {
      _that.tl
        .to(_that.moveY, 0.5, {
          value: 40,
          // ease: Back.easeOut.config(1.7),
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset, 0.6, {
          value: 30,
          ease: Back.easeOut.config(0.7),
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.5')
        .to(_that.alphaAnimation, 0.5, {
          value: 0,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.6')
        .to(_that.startAlpha, 0.5, {
          value: 0.25,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.5')
        .to(_that.moveY, 0.5, {
          value: 100,
          ease: Power3.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset, 0.5, {
          value: 10,
          ease: Power3.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.5')
        .to(_that.moveYSec, 1, {
          value: 150,
          ease: Power3.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.65')
        .to(_that.alphaAnimation, 0.35, {
          value: 1,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.5')
        .to(_that.startAlpha, 0.3, {
          value: 0,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
    }
    
    
  }
  
  drawRects () {
    let _that = this
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    for (let i = 0; i < _that.rectsQuantity; i++) {
      _that.ctx.save()
      _that.ctx.beginPath()
      // _that.ctx.globalAlpha = (1 / _that.rectsQuantity) * (_that.rectsQuantity - i)
      let getAlpha = function() {
        if ( _that.startAlpha.value - _that.alphaAnimation.value + i/4 > 0) {
          return _that.startAlpha.value - _that.alphaAnimation.value + i/4;
        } else {
          return 0;
        }
      }
      _that.ctx.globalAlpha = getAlpha();
      
      _that.ctx.moveTo(_that.canvas.width / 2 - _that.rectWidth.value / 2 + _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value - _that.rectsOffset.value * i + 60 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2, _that.canvas.height / 2 - _that.rectHeight.value / 2 + 25 - _that.moveY.value - _that.rectsOffset.value * i + 60 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2 - _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 + _that.rectHeight.value / 2 - _that.moveY.value - _that.rectsOffset.value * i + 60 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2, _that.canvas.height / 2 + _that.rectHeight.value / 2 - 25 - _that.moveY.value - _that.rectsOffset.value * i + 60 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2 + _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value - _that.rectsOffset.value * i + 60 - _that.moveYSec.value)
      _that.ctx.lineWidth = 1
      _that.ctx.strokeStyle = _that.blocksColor;
      _that.ctx.stroke()
      _that.ctx.closePath()
      _that.ctx.fillStyle = _that.blocksColor;
      _that.ctx.fill()
      _that.ctx.restore()
    }
  }
  onResize () {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
      // window.location.reload()
    })
  }
}
