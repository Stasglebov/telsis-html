import { TweenLite } from 'gsap';
import {
  getScrollBarWidth,
  currentPage
} from '../modules/dev/_helpers';

export default class Shapes {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      blocksColor: '#e72064',
      backgroundColor: 'rgba(0,0,0,0)'
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
    
  }
  
  init () {
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    this.ctx.fillStyle = 'rgba(0,0,0,0)'
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)

    if ( currentPage === 'contacts' || currentPage === 'product' ) {
      this.rectWidth = {
        value: 350
      }
      this.rectHeight = {
        value: 200
      }
      this.startAlpha = {
        value: 0
      }
      this.rectsQuantity = 8
      this.rectsSkewX = {
        value: 0.67
      }

      this.rectsSkewY = {
        value: 45
      }

      window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
      this.tlSec = new TimelineMax();
      this.tl = new TimelineMax({repeat: -1, repeatDelay: 1})
      this.tlStart = new TimelineMax()
      this.rectsOffset = [
        {
          value: 0
        },
        {
          value: 20
        },
        {
          value: 45
        },
        {
          value: 65
        },
        {
          value: 110
        },
        {
          value: 180
        },
        {
          value: 270
        },
        {
          value: 380
        }
      ]
      this.moveY = {
        value: 0
      }
      this.moveYSec = {
        value: 85
      }
      this.moveYSecHelp = {
        value: 85
      }
      if (window.innerWidth < 1600) {
        this.moveYSec = {
          value: 35
        }
        this.moveYSecHelp = {
          value: 35
        }
      }
      this.startAlpha = {
        value: 0.3
      }
      this.alphaAnimation = {
        value: 1
      }

      this.grd = this.ctx.createLinearGradient(0,0, this.canvas.width, this.canvas.height);
      this.grd.addColorStop(0, '#654298');
      this.grd.addColorStop(1, '#e12267');

      this.render()
      return;
    }

    this.prevElem = document.querySelector('.preloader');
    document.querySelector('.intro').style.paddingRight = `${getScrollBarWidth()}px`;
    let checker = setInterval( () => {
      if ( this.prevElem.classList.contains('loaded') ) {
        clearInterval(checker)
        setTimeout( () => {
          this.prevElem.remove()
        }, 1000)
        
        document.querySelector('.intro').style.paddingRight = 0;
        document.body.classList.remove('lock');
        this.rectWidth = {
          value: 350
        }
        this.rectHeight = {
          value: 200
        }
        this.startAlpha = {
          value: 0
        }
        this.rectsSkewY = {
          value: 70
        }
        if ( window.innerWidth < 767 ) {
          this.rectWidth = {
            value: 230
          }
          this.rectHeight = {
            value: 125
          }
          this.rectsSkewY = {
            value: 45
          }
        }
        this.rectsQuantity = 8
        this.rectsSkewX = {
          value: 0.67
        }
  
        
  
        window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
        let _that = this;
        this.tlSec = new TimelineMax();
        this.tl = new TimelineMax({
          repeat: 1,
          repeatDelay: 1,
          onComplete: function() {
            _that.drawOneShape();
          }
        })
        this.tlStart = new TimelineMax()
        this.rectsOffset = [
          {
            value: 0
          },
          {
            value: 20
          },
          {
            value: 45
          },
          {
            value: 65
          },
          {
            value: 110
          },
          {
            value: 180
          },
          {
            value: 270
          },
          {
            value: 380
          }
        ]
        this.moveY = {
          value: 0
        }
        this.moveYSec = {
          value: 65
        }
        this.moveYSecHelp = {
          value: 65
        }
        if (window.innerWidth < 1600) {
          this.moveYSec = {
            value: 35
          }
          this.moveYSecHelp = {
            value: 35
          }
        }
        this.startAlpha = {
          value: 0.3
        }
        this.alphaAnimation = {
          value: 1
        }
  
        this.grd = this.ctx.createLinearGradient(0,0, this.canvas.width, this.canvas.height);
        this.grd.addColorStop(0, '#654298');
        this.grd.addColorStop(1, '#e12267');
  
        this.render()
        
      }
    }, 50)
    
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  drawOneShape() {
    let _that = this;
    _that.tlSec
      .to(_that.rectsOffset[0], 1, {
        value: 0,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      })
      .to(_that.rectsOffset[1], 1, {
        value: 20,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=1')
      .to(_that.rectsOffset[2], 1, {
        value: 45,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=1')
      .to(_that.rectsOffset[3], 1, {
        value: _that.moveYSecHelp.value,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=1')
      .to(_that.rectsOffset[4], 1, {
        value: 110,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=1')
      .to(_that.rectsOffset[5], 1, {
        value: 180,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=1')
      .to(_that.rectsOffset[6], 1, {
        value: 270,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=1')
      .to(_that.rectsOffset[7], 1, {
        value: 380,
        ease: Power3.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=1')
  }
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    // _that.ctx.fillStyle = _that.backgroundColor;
    // _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    
    _that.ctx.restore()
    _that.tlStart
      .to(_that.startAlpha, 1, {
        value: 0,
        onUpdate: function () {
          _that.drawRects()
        }
      })
    if ( window.innerWidth < 1200 && window.innerWidth > 767 ) {
      _that.tl
        .from(_that.rectsOffset[0], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .from(_that.rectsOffset[1], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[2], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[3], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[4], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[5], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[6], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[7], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          },
          onComplete: function() {
            _that.tl.stop();
          }
        }, '-=1')
    } else {
      _that.tl
        .from(_that.rectsOffset[0], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .from(_that.rectsOffset[1], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[2], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[3], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[4], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[5], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[6], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .from(_that.rectsOffset[7], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        // step 2
        .to(_that.rectsOffset[7], 1, {
          value: 380,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset[0], 1, {
          value: 240,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[1], 0.9, {
          value: 280,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[2], 0.85, {
          value: 320,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.9')
        .to(_that.rectsOffset[3], 0.8, {
          value: 360,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.85')
        .to(_that.rectsOffset[4], 0.75, {
          value: 380,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.8')
        .to(_that.rectsOffset[5], 0.7, {
          value: 380,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.75')
        .to(_that.rectsOffset[6], 0.65, {
          value: 380,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        // step 3
        .to(_that.rectsOffset[0], 1, {
          value: 0,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset[1], 1, {
          value: 20,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[2], 1, {
          value: 45,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[3], 1, {
          value: _that.moveYSecHelp.value,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[4], 1, {
          value: 110,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[5], 1, {
          value: 180,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[6], 1, {
          value: 270,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[7], 1, {
          value: 380,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        // step 4
        .to(_that.rectsOffset[7], 0.7, {
          value: 80,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset[6], 0.7, {
          value: 70,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        .to(_that.rectsOffset[5], 0.7, {
          value: 50,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        .to(_that.rectsOffset[4], 0.7, {
          value: 30,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        .to(_that.rectsOffset[3], 0.7, {
          value: 0,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        .to(_that.rectsOffset[2], 0.7, {
          value: 0,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        .to(_that.rectsOffset[1], 0.7, {
          value: 0,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        .to(_that.rectsOffset[0], 0.7, {
          value: 0,
          ease: Power2.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.7')
        //step 5
        .to(_that.rectsOffset[7], 1, {
          value: 350,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset[6], 1, {
          value: 300,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[5], 1, {
          value: 250,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[4], 1, {
          value: 200,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[3], 1, {
          value: 150,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[2], 1, {
          value: 100,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[1], 1, {
          value: 50,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[0], 1, {
          value: 0,
          ease: Power2.easeOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[7], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset[6], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[5], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[4], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[3], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[2], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[1], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[0], 1, {
          value: 200,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        // step 4
        .to(_that.rectsOffset[7], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset[6], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[5], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[4], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[3], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[2], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[1], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.rectsOffset[0], 1, {
          value: 0,
          ease: Power2.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
        .to(_that.moveYSec, 1, {
          value: _that.moveYSecHelp.value,
          ease: Power3.easeIn,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=1')
    }
  }
  
  drawRects () {
    
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    for (let i = 0; i < _that.rectsQuantity; i++) {
      _that.ctx.save()
      _that.ctx.beginPath()
      _that.ctx.globalAlpha = 0.3 - _that.startAlpha.value;
      _that.ctx.globalCompositeOperation = 'xor';
      
      
      _that.ctx.moveTo(_that.canvas.width / 2 - _that.rectWidth.value / 2 + _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value + 250 - _that.moveYSec.value - _that.rectsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2, _that.canvas.height / 2 - _that.rectHeight.value / 2 + _that.rectsSkewY.value - _that.moveY.value  + 250 - _that.moveYSec.value - _that.rectsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2 - _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 + _that.rectHeight.value / 2 - _that.moveY.value + 250 - _that.moveYSec.value - _that.rectsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2, _that.canvas.height / 2 + _that.rectHeight.value / 2 - _that.rectsSkewY.value - _that.moveY.value + 250 - _that.moveYSec.value - _that.rectsOffset[i].value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2 + _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value  + 250 - _that.moveYSec.value - _that.rectsOffset[i].value)
      // _that.ctx.lineWidth = 1
      // _that.ctx.strokeStyle = _that.blocksColor;
      // _that.ctx.stroke()
      _that.ctx.closePath()
      _that.ctx.fillStyle = _that.blocksColor;
      _that.ctx.fill()
      _that.ctx.restore()
    }
  }
  onResize () {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
      // window.location.reload()
    })
  }
  
}
