import { TweenLite } from 'gsap'

export default class BtnType2 {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      color: '#fff',
      colorRGBA: {
        value: 0
      },
      blocksNumber: 4,
      shapeWidth: {
        value: 72
      },
      shapeHeight: {
        value: 40
      },
      blockOffset: {
        value: 4
      },
      offset: {
        value: 0
      },
      skewY: {
        value: 20
      }
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass;
    this.tl = new TimelineMax({paused:true});
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    this.blocksOpacity = {
      value: 0
    }
    this.crossMove = {
      value: 0
    }
    this.render()
    if ( window.innerWidth > 1200 ) {
      this.initHoverEvent()
    }
    
    
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    
    _that.tl
      .fromTo(_that.blockOffset, 0.4, {
        value: 4
      }, {
        value: 0,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      })
      .fromTo(_that.offset, 0.4, {
        value: 0
      }, {
        value: 12,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
      .fromTo(_that.skewY, 0.4, {
        value: 20
      }, {
        value: 0,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
      .fromTo(_that.shapeHeight, 0.4, {
        value: 40
      }, {
        value: 50,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
      .fromTo(_that.shapeWidth, 0.4, {
        value: 72
      }, {
        value: 50,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
      .fromTo(_that.blocksOpacity, 0.4, {
        value: 0
      }, {
        value: 1,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
      .fromTo(_that.crossMove, 0.4, {
        value: 0
      }, {
        value: 5,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
      .fromTo(_that.colorRGBA, 0.4, {
        value: 0
      }, {
        value: 255,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
    _that.drawRects()
  }
  
  drawRects () {
    
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height);
    
    // draw text degrees
    for (let i = 0; i < _that.blocksNumber; i++) {
      _that.ctx.save()
      _that.ctx.beginPath();
      _that.ctx.globalAlpha = 1
      _that.ctx.strokeStyle = _that.color;
      _that.ctx.fillStyle = `rgba(255,255,255,${_that.blocksOpacity.value})`;
      _that.ctx.moveTo( (_that.blocksNumber - 1)*_that.blockOffset.value - i*_that.blockOffset.value + _that.offset.value, i*_that.blockOffset.value + 0.5);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value + _that.shapeWidth.value - i*_that.blockOffset.value - _that.skewY.value + _that.offset.value, i*_that.blockOffset.value + 0.5);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value + _that.shapeWidth.value - i*_that.blockOffset.value + _that.offset.value, _that.shapeHeight.value + i*_that.blockOffset.value + 0.5);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value - i*_that.blockOffset.value + _that.skewY.value + _that.offset.value, _that.shapeHeight.value + i*_that.blockOffset.value + 0.5);
      _that.ctx.closePath();
      _that.ctx.stroke();
      _that.ctx.fill();
      _that.ctx.restore();
    }
  
    // draw line 1
      
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.moveTo(42.5 - _that.crossMove.value, 18.5);
      _that.ctx.lineTo(42.5 - _that.crossMove.value, 35.5);
      _that.ctx.strokeStyle = `rgba(${255 - _that.colorRGBA.value},${255 - _that.colorRGBA.value},${255 - _that.colorRGBA.value},1)`;
      _that.ctx.lineWidth = 1;
      _that.ctx.stroke();
      _that.ctx.closePath();
      _that.ctx.restore();
      // draw line 2
      _that.ctx.save();
      _that.ctx.beginPath();
      _that.ctx.moveTo(34.5 - _that.crossMove.value, 26.5);
      _that.ctx.lineTo(51.5 - _that.crossMove.value, 26.5);
      _that.ctx.strokeStyle = `rgba(${255 - _that.colorRGBA.value},${255 - _that.colorRGBA.value},${255 - _that.colorRGBA.value},1)`;
      _that.ctx.lineWidth = 1;
      _that.ctx.stroke();
      _that.ctx.closePath();
      _that.ctx.restore();
    
  }
  
  
  initHoverEvent() {
    let _that = this;
    _that.container.parentElement.parentElement.parentElement.addEventListener('mouseover', function() {
      this.style.cursor = 'pointer';
      _that.tl.play();
    })
    _that.container.parentElement.parentElement.parentElement.addEventListener('mouseleave', function() {
      this.style.cursor = 'default';
      _that.tl.reverse();
    })
  }
  onResize () {
    let _that = this
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.init()
      // window.location.reload()
    })
  }
}
