import { TweenLite } from 'gsap'

export default class SolutionsSlider {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      firstColor: '#6e22e8',
      secColor: '#61bdee',
      radius: {
        value: 200
      },
      containerLeftClass: '.operators-list',
      containerRightClass: '.contacts-list',
      dotRadius: 7,
      dotsLeftQuantity: 6,
      dotsRightQuantity: 8
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    
    this.activeRadius = {
      value: 0
    }
    this.activeRadiusNew = {
      value: 15
    }
    
    this.smallOffset = 25;
    this.largeOffset = 50;
    if ( window.innerWidth < 767 ) {
      this.smallOffset = -5;
      this.largeOffset = 10;
      this.dotRadius = 4;
      this.activeRadiusNew = {
        value: 7
      }
    }
    this.innerElems = [...document.querySelectorAll('.solutions-slider .center .inner')]
    this.elemsLeft = [...document.querySelectorAll(`${this.containerLeftClass} li`)]
    this.elemsRight = [...document.querySelectorAll(`${this.containerRightClass} li`)]
    this.dotsLeftQuantity = this.elemsLeft.length
    this.dotsRightQuantity = this.elemsRight.length
    this.elemsAll = []
    
    for (let i = 0; i < this.elemsRight.length; i++) {
      this.elemsAll.push(this.elemsRight[i])
    }
    for (let i = 0; i < this.elemsLeft.length; i++) {
      this.elemsAll.push(this.elemsLeft[i])
    }
    
    if (window.innerWidth < 1600 && window.innerWidth > 767) {
      this.radius.value = 210
      
    } else if (window.innerWidth < 767) {
      this.radius.value = 140
      
    }
    
    this.allowHover = true
    this.dotsData = []
    this.angleRight = Math.PI / (this.dotsRightQuantity + 1)
    this.angleLeft = -Math.PI / (this.dotsLeftQuantity + 1)
    
    this.activeDotNumber = 0
    
    this.dots = []
    this.dot = {}
    this.activeIndex = null
    this.tl = new TimelineMax()
    this.tlActive = new TimelineMax()
    for (let i = 0; i < this.dotsRightQuantity; i++) {
      
      let dot = {}
      dot.x = (this.radius.value + this.smallOffset) * Math.cos(this.angleRight * i + this.angleRight + 1.5 * Math.PI)
      dot.y = (this.radius.value + this.smallOffset) * Math.sin(this.angleRight * i + this.angleRight + 1.5 * Math.PI)
      dot.radius = this.dotRadius
      dot.angle = this.angleRight * i * 180 / Math.PI
      this.dots.push(dot)
      
    }
    for (let i = 0; i < this.dotsLeftQuantity; i++) {
      
      let dot = {}
      dot.x = (this.radius.value + this.largeOffset) * Math.cos(this.angleLeft * i + this.angleLeft + 1.5 * Math.PI)
      dot.y = (this.radius.value + this.largeOffset) * Math.sin(this.angleLeft * i + this.angleLeft + 1.5 * Math.PI)
      dot.radius = this.dotRadius
      dot.angle = this.angleLeft * i * 180 / Math.PI
      this.dots.push(dot)
      
    }
    
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay()
    this.grd = this.ctx.createLinearGradient(this.canvas.width / 2 - this.radius.value / 2, this.canvas.height / 2 - this.radius.value / 2, this.canvas.width / 2 + this.radius.value / 2, this.canvas.height / 2 + this.radius.value / 2)
    this.grd.addColorStop(0, this.firstColor)
    this.grd.addColorStop(1, this.secColor)
    // grd inner
    this.grdInner = this.ctx.createLinearGradient(this.canvas.width / 2 - this.radius.value / 2, this.canvas.height / 2 + this.radius.value / 2, this.canvas.width / 2 + this.radius.value / 2, this.canvas.height / 2 - this.radius.value / 2)
    this.grdInner.addColorStop(0, this.firstColor)
    this.grdInner.addColorStop(1, this.secColor)
    
    this.render()
    this.initClickEvent()
    this.initListHoverEvent()
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    
    let _that = this
    
    requestAnimationFrame(function () {
      _that.drawRects
    })
    
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.restore()
    _that.drawRects()
  }
  
  drawRects () {
    
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    // draw small out circle
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value + _that.smallOffset, -Math.PI / 2, Math.PI / 2)
    _that.ctx.lineWidth = 1
    _that.ctx.strokeStyle = '#ccc'
    _that.ctx.stroke()
    _that.ctx.closePath()
    _that.ctx.fillStyle = 'transparent'
    _that.ctx.fill()
    _that.ctx.restore()
    
    // draw small out circle white
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value + _that.smallOffset, Math.PI / 2, -Math.PI / 2)
    _that.ctx.lineWidth = 1
    _that.ctx.strokeStyle = '#fff'
    _that.ctx.stroke()
    _that.ctx.closePath()
    _that.ctx.fillStyle = 'transparent'
    _that.ctx.fill()
    _that.ctx.restore()
    
    // draw big out circle
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value + _that.largeOffset, -Math.PI / 2, Math.PI / 2)
    _that.ctx.lineWidth = 1
    _that.ctx.strokeStyle = '#fff'
    _that.ctx.stroke()
    _that.ctx.closePath()
    _that.ctx.fillStyle = 'transparent'
    _that.ctx.fill()
    _that.ctx.restore()
    
    // draw big out circle white
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value + _that.largeOffset, Math.PI / 2, -Math.PI / 2)
    _that.ctx.lineWidth = 1
    _that.ctx.strokeStyle = '#ccc'
    _that.ctx.stroke()
    _that.ctx.closePath()
    _that.ctx.fillStyle = 'transparent'
    _that.ctx.fill()
    _that.ctx.restore()
    
    // draw inner circle
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value - 25, 0, 2 * Math.PI)
    _that.ctx.closePath()
    _that.ctx.fillStyle = _that.grdInner
    _that.ctx.fill()
    _that.ctx.restore()
    
    // draw dots
    _that.dotsData = []
    for (let i = 0; i < _that.dots.length; i++) {
      _that.ctx.save()
      _that.ctx.beginPath()
      if (_that.activeDotNumber === i) {
        _that.activeIndex = i
        
        _that.grdActive = _that.ctx.createLinearGradient(_that.canvas.width / 2 + _that.dots[_that.activeIndex].x - _that.activeRadius.value + 10, _that.canvas.height / 2 + _that.dots[_that.activeIndex].y + _that.activeRadius.value + 10, _that.canvas.width / 2 + _that.dots[_that.activeIndex].x - _that.activeRadius.value - 10, _that.canvas.height / 2 + _that.dots[_that.activeIndex].y - _that.activeRadius.value - 10)
        _that.grdActive.addColorStop(0, _that.secColor)
        _that.grdActive.addColorStop(1, _that.firstColor)
        
        _that.tlActive.time(0)
        _that.tlActive
          .fromTo(_that.activeRadius, 1, {
            value: _that.dotRadius
          }, {
            value: _that.activeRadiusNew.value,
            ease: Power2.easeInOut,
            delay: 0,
            onUpdate: function () {
              _that.drawActiveCircle()
            }
          })
      } else {
        
        _that.ctx.arc(_that.canvas.width / 2 + _that.dots[i].x, _that.canvas.height / 2 + _that.dots[i].y, _that.dots[i].radius, 0, 2 * Math.PI)
      }
      _that.ctx.closePath()
      _that.ctx.fillStyle = '#fff'
      _that.ctx.fill()
      _that.ctx.restore()
    }
    
  }
  
  drawActiveCircle () {
    let _that = this
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2 + _that.dots[_that.activeIndex].x, _that.canvas.height / 2 + _that.dots[_that.activeIndex].y, _that.activeRadius.value, 0, 2 * Math.PI)
    _that.ctx.filter = 'blur(0.5px)'
    _that.ctx.fillStyle = _that.grdActive
    _that.ctx.fill()
    _that.ctx.closePath()
    _that.ctx.restore()
  }
  
  showActiveInnerElem () {
    let _that = this
    for (let i = 0; i < _that.innerElems.length; i++) {
      _that.innerElems[i].classList.remove('active')
      _that.elemsAll[i].classList.remove('active')
    }
    _that.innerElems[_that.activeDotNumber].classList.add('active')
    _that.elemsAll[_that.activeDotNumber].classList.add('active')
  }
  
  hideAllInnerElem () {
    let _that = this
    for (let i = 0; i < _that.innerElems.length; i++) {
      _that.innerElems[i].classList.remove('active')
    }
  }
  
  initClickEvent () {
    let _that = this
    
    let indexX = null
    _that.container.addEventListener('mousemove', function (e) {
      let hoverChecker = setTimeout(function () {
        _that.container.style.cursor = 'default'
      }, 100)
      for (let i = 0; i < _that.dots.length; i++) {
        let x = _that.canvas.width / 2 + _that.dots[i].x - e.layerX
        let y = _that.canvas.height / 2 + _that.dots[i].y - e.layerY
        if (window.innerWidth < 1199) {
          if (x * x + y * y <= _that.dotRadius * _that.dotRadius + 20) {
            
            _that.container.style.cursor = 'pointer'
            clearTimeout(hoverChecker)
          }
        } else {
          if (x * x + y * y <= _that.dotRadius * _that.dotRadius) {
            _that.container.style.cursor = 'pointer'
            clearTimeout(hoverChecker)
          }
        }
        
      }
      
    })
    
    _that.container.addEventListener('touchstart', function (e) {
      let findIndex = null
      
      for (let i = 0; i < _that.dots.length; i++) {
        let x = _that.canvas.width / 2 + _that.dots[i].x - e.layerX
        let y = _that.canvas.height / 2 + _that.dots[i].y - e.layerY
        
        if (x * x + y * y <= _that.dotRadius * _that.dotRadius * 5 ) {
          findIndex = i
          _that.activeDotNumber = findIndex
          
          _that.render()
          _that.showActiveInnerElem()
          return
        } else {
          findIndex = null
        }
      }
      
    })
  
    _that.container.addEventListener('click', function (e) {
      let findIndex = null
    
      for (let i = 0; i < _that.dots.length; i++) {
        let x = _that.canvas.width / 2 + _that.dots[i].x - e.layerX;
        let y = _that.canvas.height / 2 + _that.dots[i].y - e.layerY;
      
        if (x * x + y * y <= _that.dotRadius * _that.dotRadius ) {
          findIndex = i
          _that.activeDotNumber = findIndex
        
          _that.render()
          _that.showActiveInnerElem()
          return
        } else {
          findIndex = null
        }
      }
    
    })
  }
  
  initListHoverEvent () {
    let _that = this
    for (let i = 0; i < this.elemsAll.length; i++) {
      this.elemsAll[i].addEventListener('mouseenter', function () {
        if (_that.allowHover) {
          _that.activeDotNumber = i
          _that.render()
          _that.showActiveInnerElem()
          _that.allowHover = true
        }
      })
      this.elemsAll[i].addEventListener('mouseleave', function () {
        if (_that.allowHover) {
          _that.hideAllInnerElem()
        }
      })
      
      this.elemsAll[i].addEventListener('click', function (e) {
        _that.activeDotNumber = i
        e.preventDefault()
        if (!_that.allowHover) {
          _that.render()
          _that.showActiveInnerElem()
        }
        _that.allowHover = false
      })
    }
  }
  
  onResize () {
    let _that = this
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.init()
      // window.location.reload()
    })
  }
}
