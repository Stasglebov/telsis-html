import { TweenLite } from 'gsap'

export default class GradientBackground {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      firstColor: 'rgba(101,66,152,1)',
      secondColor: 'rgba(225,34,103,1)'
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    if (window.innerWidth > 1200) {
      this.onResize()
      this.init()
      this.initMouseMoveEvent()
    }
    
  }
  
  init () {
    
    this.xPosition = {
      value: 0
    }
    this.yPosition = {
      value: 0
    }
    
    this.tl = new TimelineMax()
    
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay()
    
    this.render()
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor
    _that.ctx.restore()
    
    _that.drawRects()
    
  }
  
  drawRects () {
    let _that = this
    
    _that.grd = this.ctx.createLinearGradient(0 - _that.xPosition.value / 2, 0, this.canvas.width + _that.xPosition.value / 2, this.canvas.height)
    _that.grd.addColorStop(0, this.firstColor)
    // _that.grd.addColorStop(_that.xPosition.value, '#a2327f')
    _that.grd.addColorStop(1, this.secondColor)
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.fillStyle = _that.grd
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.closePath()
    _that.ctx.restore()
  }
  
  initMouseMoveEvent () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    document.querySelector('.intro').addEventListener('mousemove', function (e) {
      _that.xPosition.value = e.pageX
      _that.yPosition.value = e.pageY
      
      _that.drawRects()
    })
  }
  
  onResize () {
    let _that = this
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
    })
  }
}
