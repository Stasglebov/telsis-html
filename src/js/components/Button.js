import { TweenLite } from 'gsap';
import Flickity  from 'flickity';


export default class Btn {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      color: '#ea2066',
      blocksNumber: 4,
      shapeWidth: 72,
      shapeHeight: 40,
      blockOffset: {
        value: 4
      },
      direction: 'left',
      skewX: 20,
      offset: {
        value: 0
      }
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass;
    this.tl = new TimelineMax({paused:true});
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    this.options = {
      pageDots: false,
      accessibility: false,
      cellAlign: 'left',
      initialIndex: 0,
      groupCells: 4
    }
    if ( window.innerWidth > 767 && window.innerWidth < 1200 ) {
      this.options.groupCells = 2;
    }
    if ( window.innerWidth <= 767 ) {
      this.shapeWidth = 50;
      this.shapeHeight = 25;
      this.skewX = 15;
      this.options.groupCells = 1;
    }
    
    this.flickty = new Flickity('.slider-clients', this.options);
    
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
  
    this.render()
    if ( window.innerWidth > 1200) {
      
      this.initHoverEvent()
      
    }
    
    this.initFlickChangeEvent()
    
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    
    _that.tl
      .fromTo(_that.blockOffset, 0.4, {
        value: 4
      }, {
        value: 0,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      })
      .fromTo(_that.offset, 0.4, {
        value: 0
      }, {
        value: 12,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        },
        onComplete: function() {
          _that.tl.pause()
        }
      }, '-=0.4')
    // _that.drawRects()
  }
  
  drawRects () {
    
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height);
    
    // draw text degrees
    for (let i = 0; i < _that.blocksNumber; i++) {
      _that.ctx.save()
      _that.ctx.beginPath();
      _that.ctx.globalAlpha = 1 - ( 1 / _that.blocksNumber )*i;
      if ( _that.container.getAttribute('disabled') != null ) {
        _that.ctx.strokeStyle = 'rgba(0,0,0,0.4)';
        _that.ctx.fillStyle = 'rgba(0,0,0,0)';
      } else {
        _that.ctx.strokeStyle = 'rgba(0,0,0,0)';
        _that.ctx.fillStyle = _that.color;
      }
      
      _that.ctx.moveTo( (_that.blocksNumber - 1)*_that.blockOffset.value - i*_that.blockOffset.value + _that.offset.value, i*_that.blockOffset.value);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value + _that.shapeWidth - i*_that.blockOffset.value - _that.skewX + _that.offset.value, i*_that.blockOffset.value);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value + _that.shapeWidth - i*_that.blockOffset.value + _that.offset.value, _that.shapeHeight + i*_that.blockOffset.value);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value - i*_that.blockOffset.value + _that.skewX + _that.offset.value, _that.shapeHeight + i*_that.blockOffset.value);
      _that.ctx.closePath();
      _that.ctx.fill();
      _that.ctx.stroke();
      _that.ctx.restore();
    }
  
    // draw arrow
  
    _that.ctx.save();
    _that.ctx.beginPath();
    
    if ( _that.container.getAttribute('disabled') != null ) {
      _that.ctx.strokeStyle = 'rgba(0,0,0,0.4)';
      _that.ctx.fillStyle = 'rgba(0,0,0,0)';
    } else {
      _that.ctx.fillStyle = 'rgba(0,0,0,0)';
      _that.ctx.strokeStyle = '#fff';
    }
    if ( _that.direction === 'left' ) {
      if ( window.innerWidth < 767 ) {
        _that.ctx.moveTo(40, 5);
        _that.ctx.lineTo(33, 12);
        _that.ctx.lineTo(40, 20);
      } else {
        _that.ctx.moveTo(50, 15);
        _that.ctx.lineTo(43, 22);
        _that.ctx.lineTo(50, 30);
      }
    } else {
      if ( window.innerWidth < 767 ) {
        _that.ctx.moveTo(33, 5);
        _that.ctx.lineTo(40, 12);
        _that.ctx.lineTo(33, 20);
      } else {
        _that.ctx.moveTo(43, 15);
        _that.ctx.lineTo(50, 22);
        _that.ctx.lineTo(43, 30);
      }
      
      
    }
    _that.ctx.lineWidth = 1;
    _that.ctx.stroke();
    _that.ctx.closePath();
    _that.ctx.restore();
    
    
    
  }
  
  
  initFlickChangeEvent() {
    this.flickty.on( 'change', ( index ) => {
      this.tl.time(0)
      this.drawRects()
    });
  }
  
  initHoverEvent() {
    let _that = this;
    _that.container.addEventListener('mouseover', function() {
      this.style.cursor = 'pointer';
      _that.tl.play();
    })
    _that.container.addEventListener('mouseleave', function() {
      this.style.cursor = 'default';
      _that.tl.reverse();
    })
  }
  onResize () {
    let _that = this
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.init()
      // window.location.reload()
    })
  }
}
