import {
  getScrollBarWidth,
  currentPage,
  throttle
} from '../modules/dev/_helpers'
import { TweenLite } from 'gsap';

export default class Menu {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      blocksColor: '#fff',
      backgroundColor: 'rgba(0,0,0,0)',
      isAnimated: false,
      parentContainer: '.burger',
      navClass: '.nav'
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass;
    this.container = document.querySelector(containerClass);
    this.parentElement = document.querySelector(this.parentContainer);
    this.navElem = document.querySelector(this.navClass);
    this.canvas = {};
    this.canvas.elem = document.createElement('canvas');
    
    this.onResize();
    this.init();
    this.initClickEvent();
    this.initScrollEvent();
  }
  
  init () {
    let _that = this;
    this.rectWidth = {
      value: 44
    }
    this.rectHeight = {
      value: 25
    }
    this.rectsQuantity = 4
    this.rectsSkewX = {
      value: 0.65
    }
    this.rectsSkewY = {
      value: 25
    }
    // this.tlStart = new TimelineMax();
    this.tl = new TimelineMax({paused: true})
    this.rectsOffset = {
      value: 9
    }
    this.moveY = {
      value: 0
    }
    this.moveYSec = {
      value: -13
    }
    this.fillOpacity = {
      value: 0
    }
    this.fillColorStyle = '255'
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    
    this.render();
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    _that.ctx.restore()
    
      _that.tl
        .to(_that.moveYSec, 0.3, {
          value: 0,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        })
        .to(_that.rectsOffset, 0.3, {
          value: 0,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
        .to(_that.fillOpacity, 0.3, {
          value: 1,
          ease: Power3.easeInOut,
          onUpdate: function () {
            _that.drawRects()
          }
        }, '-=0.3')
  
    _that.drawRects()
    
  }
  
  drawRects () {
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    for (let i = 0; i < _that.rectsQuantity; i++) {
      
      _that.ctx.save()
      _that.ctx.beginPath()
      
      _that.ctx.globalAlpha = 1;
  
      _that.ctx.moveTo(_that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value - _that.rectsOffset.value * i + 0 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2, _that.canvas.height / 2 - _that.rectHeight.value / 2 + 10 - _that.moveY.value - _that.rectsOffset.value * i + 0 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 + _that.rectWidth.value / 2 - _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 + _that.rectHeight.value / 2 - _that.moveY.value - _that.rectsOffset.value * i + 0 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2, _that.canvas.height / 2 + _that.rectHeight.value / 2 - 10 - _that.moveY.value - _that.rectsOffset.value * i + 0 - _that.moveYSec.value)
      _that.ctx.lineTo(_that.canvas.width / 2 - _that.rectWidth.value / 2 + _that.rectWidth.value * _that.rectsSkewX.value, _that.canvas.height / 2 - _that.rectHeight.value / 2 - _that.moveY.value - _that.rectsOffset.value * i + 0 - _that.moveYSec.value)
      _that.ctx.lineWidth = 1
      _that.ctx.strokeStyle = _that.blocksColor;
      _that.ctx.stroke()
      _that.ctx.fillStyle = `rgba(${_that.fillColorStyle},${_that.fillColorStyle},${_that.fillColorStyle},${_that.fillOpacity.value})`;
      _that.ctx.fill();
      _that.ctx.closePath()
      // _that.ctx.fillStyle = _that.blocksColor;
      // _that.ctx.fill()
      _that.ctx.restore()
    }
  }
  
  initClickEvent() {
    let _that = this;
    
    _that.parentElement.addEventListener('click', function(e) {
      e.preventDefault();
      if ( _that.navElem.classList.contains('nav-open') ) {
        _that.tl.reverse();
        document.documentElement.classList.remove('lock');
        document.querySelector('.header').style.paddingRight = 0;
        document.querySelector('main').style.paddingRight = 0;
        _that.navElem.classList.remove('nav-open');
        document.querySelector('.burger .text').innerText = 'menu'
      } else {
        _that.tl.play();
        _that.navElem.classList.add('nav-open');
        document.querySelector('.header').style.paddingRight = `${getScrollBarWidth()}px`;
        document.querySelector('main').style.paddingRight = `${getScrollBarWidth()}px`;
        document.documentElement.classList.add('lock');
        document.querySelector('.burger .text').innerText = 'close'
      }
    })
  }
  
  initScrollEvent() {
    let _that = this;
    // tempo
    if ( currentPage === 'solutions' || currentPage === 'about' ) {
      window.addEventListener('scroll', (e) => {
        let scrolled = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
        if ( scrolled > 40 ) {
          document.querySelector('.header').classList.add('active-sub-open');
        } else {
          document.querySelector('.header').classList.remove('active-sub-open');
        }
      });
      document.querySelector('.header').classList.add('active-sub');
      _that.blocksColor = '#171617';
      _that.fillColorStyle = '17';
      _that.drawRects();
    }
    if ( currentPage === 'product' || currentPage === 'contacts' ) {
      window.addEventListener('scroll', (e) => {
        let scrolled = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
  
        if ( scrolled > 50) {
          document.querySelector('.header').classList.add('active');
          _that.blocksColor = '#171617';
          _that.fillColorStyle = '17';
          _that.drawRects();
        } else {
          document.querySelector('.header').classList.remove('active');
          _that.blocksColor = '#fff';
          _that.fillColorStyle = '255';
          _that.drawRects();
        }
      })
    }
    
    let toggle = function() {
      if ( window.pageYOffset > 150 ) {
        document.querySelector('.header').classList.add('active');
        _that.blocksColor = '#171617';
        _that.fillColorStyle = '17';
        _that.drawRects();
      } else {
        if ( window.isNowScrolled ) return;
        document.querySelector('.header').classList.remove('active');
        _that.blocksColor = '#fff';
        _that.fillColorStyle = '255';
        _that.drawRects();
      }
    }
    
    if ( currentPage !== 'home' ) return;
    let initedWithThrottle = throttle(toggle, 100);
    window.addEventListener('scroll', initedWithThrottle, true);
    
    // window.addEventListener('scroll', (e) => {
    //   if ( window.pageYOffset > 150 ) {
    //     document.querySelector('.header').classList.add('active');
    //     _that.blocksColor = '#171617';
    //     _that.fillColorStyle = '17';
    //     _that.drawRects();
    //   } else {
    //     if ( window.isNowScrolled ) return;
    //     document.querySelector('.header').classList.remove('active');
    //     _that.blocksColor = '#fff';
    //     _that.fillColorStyle = '255';
    //     _that.drawRects();
    //   }
    // })
  }
  
  onResize () {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
      // window.location.reload()
    })
  }
}
