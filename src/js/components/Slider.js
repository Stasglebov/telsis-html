import { TweenLite } from 'gsap'
import Flickity  from 'flickity';

export default class Slider {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      firstColor: '#6e11e7',
      secColor: '#649ced',
      radius: {
        value: 235
      },
      dotRadius: 4,
      dotsQuantity: 7,
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.areaX = [-4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8]
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    
    this.activeRadius = {
      value: 0
    }
    this.textDegsFontSize = {
      value: 140
    }
    this.textFontSize = {
      value: 22
    }
    if (window.innerWidth > 1600) {
      this.radius.value = 250;
    } else if (window.innerWidth < 1600 && window.innerWidth > 767 && window.innerHeight > 799){
      this.radius.value = 236;
    } else if (window.innerHeight > 767 && window.innerWidth < 1600) {
      this.radius.value = 215;
    } else if ( window.innerWidth < 767 ) {
      this.radius.value = 150
      this.textDegsFontSize = {
        value: 90
      }
      this.textFontSize = {
        value: 18
      }
    }
    this.flick = new Flickity('.slider-thumb', {
      pageDots: false,
      accessibility: false,
      cellAlign: '0.35',
      wrapAround: true,
      initialIndex: 0,
      friction: 0.6,
      selectedAttraction: 0.15
      // autoPlay: 3500
    })
    this.flickTrigger = true;
    
    this.dotsData = []
    this.angle = 2 * Math.PI / this.dotsQuantity
    this.activeDotNumber = 1
    this.progress = {
      value: 0,
      newValue: this.angle * this.activeDotNumber * 180 / Math.PI
    }
    
    this.dots = []
    this.dot = {}
    this.activeIndex = null
    this.tl = new TimelineMax()
    this.tlActive = new TimelineMax()
    for (let i = 0; i < this.dotsQuantity; i++) {
      
      let dot = {}
      dot.x = this.radius.value * Math.cos(this.angle * i + 1.5 * Math.PI)
      dot.y = this.radius.value * Math.sin(this.angle * i + 1.5 * Math.PI)
      dot.radius = this.dotRadius;
      dot.angle = this.angle * i * 180 / Math.PI
      this.dots.push(dot)
      
    }
    
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay()
    this.grd = this.ctx.createLinearGradient(this.canvas.width / 2 - this.radius.value / 2, this.canvas.height / 2 - this.radius.value / 2, this.canvas.width / 2 + this.radius.value / 2, this.canvas.height / 2 + this.radius.value / 2)
    this.grd.addColorStop(0, this.firstColor)
    this.grd.addColorStop(1, this.secColor)
    // grd inner
    this.grdInner = this.ctx.createLinearGradient(this.canvas.width / 2 - this.radius.value / 2, this.canvas.height / 2 + this.radius.value / 2, this.canvas.width / 2 + this.radius.value / 2, this.canvas.height / 2 - this.radius.value / 2)
    this.grdInner.addColorStop(0, this.firstColor)
    this.grdInner.addColorStop(1, this.secColor)
    this.render()
    // this.initEvents()
    this.initClickEvent()
    this.initFlickChangeEvent()
    this.initFlickEvent()
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    
    let _that = this
    _that.progress.newValue = _that.angle * _that.activeDotNumber * 180 / Math.PI
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.restore()
    _that.tl
      .fromTo(_that.progress, 1, {
        value: _that.progress.value
      }, {
        value: _that.progress.newValue,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        },
        onComplete: function () {
          _that.progress.value = _that.progress.newValue;
          _that.flickTrigger = true;
        }
      })
    _that.drawRects()
  }
  
  drawRects () {
    
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    // draw out circle
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value, 0, 2 * Math.PI)
    _that.ctx.lineWidth = 1
    _that.ctx.strokeStyle = '#ccc'
    _that.ctx.stroke()
    _that.ctx.closePath()
    _that.ctx.fillStyle = 'transparent'
    _that.ctx.fill()
    _that.ctx.restore()
    // draw progress
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value, 1.5 * Math.PI, _that.progress.value * Math.PI / 180 + 1.5 * Math.PI)
    _that.ctx.lineWidth = 2
    _that.ctx.strokeStyle = _that.grd
    _that.ctx.stroke()
    _that.ctx.closePath()
    _that.ctx.fillStyle = 'transparent'
    _that.ctx.fill()
    _that.ctx.restore()
    // draw inner circle
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2, _that.canvas.height / 2, _that.radius.value - 25, 0, 2 * Math.PI)
    _that.ctx.closePath()
    _that.ctx.fillStyle = _that.grdInner
    _that.ctx.fill()
    _that.ctx.restore()
    // draw dots
    _that.dotsData = []
    for (let i = 0; i < _that.dots.length; i++) {
      _that.ctx.save()
      _that.ctx.beginPath()
      if (_that.activeDotNumber === i ) {
        // if ( _that.activeDotNumber === _that.dots.length) {
        //   _that.activeIndex = 0;
        // } else {
          _that.activeIndex = i;
        // }
        
        
        _that.tlActive.time(0);
        _that.tlActive
          .fromTo(_that.activeRadius, 1, {
            value: 4
          }, {
            value: 15,
            ease: Power2.easeInOut,
            delay: 0,
            onUpdate: function () {
              _that.drawActiveCircle()
            }
          })
      } else {
        
          _that.ctx.arc(_that.canvas.width / 2 + _that.dots[i].x, _that.canvas.height / 2 + _that.dots[i].y, _that.dots[i].radius, 0, 2 * Math.PI)
        if ( _that.activeDotNumber === _that.dots.length) {
          _that.activeIndex = 0;
          _that.tlActive.time(0);
          _that.tlActive
            .fromTo(_that.activeRadius, 1, {
              value: 4
            }, {
              value: 15,
              ease: Power2.easeInOut,
              delay: 0,
              onUpdate: function () {
                _that.drawActiveCircle()
              }
            })
        }
        
      }
      // _that.ctx.arc(_that.canvas.width/2 + _that.dots[i].x , _that.canvas.height/2 + _that.dots[i].y, _that.dots[i].radius, 0, 2*Math.PI )
      _that.ctx.closePath()
      _that.ctx.fillStyle = '#fff'
      _that.ctx.fill()
      _that.ctx.restore()
    }
    
    
    if ( window.innerWidth > 767 ) {
      // draw text degrees
      _that.ctx.save()
      _that.ctx.globalAlpha = 0.4
      _that.ctx.fillStyle = '#333'
      _that.ctx.textAlign = 'center'
      _that.ctx.textBaseline = 'middle'
      _that.ctx.font = `140px ApercuBold`
      _that.ctx.fillText(`${Math.ceil(_that.progress.value)}`, _that.canvas.width / 2, _that.canvas.height / 2 - 15)
      _that.ctx.restore()
  
      // graw degrees
      _that.ctx.save()
      _that.ctx.globalAlpha = 0.4
      _that.ctx.fillStyle = '#333'
      _that.ctx.textAlign = 'center'
      _that.ctx.textBaseline = 'middle'
      _that.ctx.font = `140px RobotoBold`
      if ( _that.progress.value < 100 ) {
        _that.ctx.fillText('°', _that.canvas.width / 2 + 110, _that.canvas.height / 2 - 15)
      } else {
        _that.ctx.fillText('°', _that.canvas.width / 2 + 145, _that.canvas.height / 2 - 15)
      }
      _that.ctx.restore()
  
      // draw subText
      _that.ctx.save()
      _that.ctx.globalAlpha = 0.8
      _that.ctx.fillStyle = '#333'
      _that.ctx.textAlign = 'center'
      _that.ctx.textBaseline = 'middle'
      _that.ctx.font = `18px ApercuBold`
      _that.ctx.fillText(`360° solutions for`, _that.canvas.width / 2, _that.canvas.height / 2 + 85)
      _that.ctx.fillText(`your business`, _that.canvas.width / 2, _that.canvas.height / 2 + 110)
      _that.ctx.restore();
    } else {
      // draw text degrees
      _that.ctx.save()
      _that.ctx.globalAlpha = 0.4
      _that.ctx.fillStyle = '#333'
      _that.ctx.textAlign = 'center'
      _that.ctx.textBaseline = 'middle'
      _that.ctx.font = `90px ApercuBold`
      _that.ctx.fillText(`${Math.ceil(_that.progress.value)}`, _that.canvas.width / 2, _that.canvas.height / 2 - 15)
      _that.ctx.restore()
  
      // graw degrees
      _that.ctx.save()
      _that.ctx.globalAlpha = 0.4
      _that.ctx.fillStyle = '#333'
      _that.ctx.textAlign = 'center'
      _that.ctx.textBaseline = 'middle'
      _that.ctx.font = `90px RobotoBold`
      if ( _that.progress.value < 100 ) {
        _that.ctx.fillText('°', _that.canvas.width / 2 + 70, _that.canvas.height / 2 - 15)
      } else {
        _that.ctx.fillText('°', _that.canvas.width / 2 + 95, _that.canvas.height / 2 - 15)
      }
      _that.ctx.restore()
  
      // draw subText
      _that.ctx.save()
      _that.ctx.globalAlpha = 0.8
      _that.ctx.fillStyle = '#333'
      _that.ctx.textAlign = 'center'
      _that.ctx.textBaseline = 'middle'
      _that.ctx.font = `18px ApercuBold`
      _that.ctx.fillText(`360° solutions for`, _that.canvas.width / 2, _that.canvas.height / 2 + 60)
      _that.ctx.fillText(`your business`, _that.canvas.width / 2, _that.canvas.height / 2 + 85)
      _that.ctx.restore();
    }
    
    
    
    document.querySelector('.slider-item.is-selected .sliderValue').innerHTML = `${Math.ceil(_that.progress.value)}°`;
    
  }
  
  drawActiveCircle () {
    let _that = this
    if (!_that.activeIndex) return
    _that.ctx.save()
    _that.ctx.beginPath()
    _that.ctx.arc(_that.canvas.width / 2 + _that.dots[_that.activeIndex].x, _that.canvas.height / 2 + _that.dots[_that.activeIndex].y, _that.activeRadius.value, 0, 2 * Math.PI)
    _that.ctx.fillStyle = '#fff'
    _that.ctx.fill()
    _that.ctx.closePath()
    _that.ctx.restore()
  }
  
  // initEvents () {
  //   let _that = this
  //   let nextBtn = document.querySelector('.flickity-prev-next-button.next')
  //   let prevBtn = document.querySelector('.flickity-prev-next-button.previous')
  //   nextBtn.addEventListener('click', function () {
  //     if (_that.activeDotNumber === _that.dotsQuantity) {
  //       _that.activeDotNumber = 1
  //       _that.render()
  //       return
  //     }
  //
  //     _that.activeDotNumber++
  //     _that.render()
  //   })
  //   prevBtn.addEventListener('click', function () {
  //     if (_that.activeDotNumber === 1) {
  //       _that.activeDotNumber = _that.dotsQuantity;
  //       console.log(_that.activeDotNumber)
  //       _that.render()
  //       return
  //     }
  //     _that.activeDotNumber--
  //     _that.render()
  //   })
  // }
  initFlickChangeEvent() {
    this.flick.on( 'change', ( index ) => {
      if ( !this.flickTrigger ) return;
      this.activeDotNumber = index + 1
      this.tl.time(100)
      this.render()
    });
  }
  
  initFlickEvent() {
    this.flick.on( 'staticClick', ( event, pointer, cellElement, cellIndex ) => {
      this.tl.time(100)
      this.flick.select( cellIndex, false, false )
      this.activeDotNumber = cellIndex + 1;
      // this.render()
    });
  }
  
  initClickEvent () {
    let _that = this
    
    _that.container.addEventListener('mousemove', function (e) {
      let hoverChecker = setTimeout(function () {
        _that.container.style.cursor = 'default'
      }, 100)
      for (let i = 0; i < _that.dots.length; i++) {
        let x = _that.canvas.width / 2 + _that.dots[i].x - e.layerX
        let y = _that.canvas.height / 2 + _that.dots[i].y - e.layerY
        if (x * x + y * y <= _that.dotRadius*_that.dotRadius) {
          _that.container.style.cursor = 'pointer'
          clearTimeout(hoverChecker);
        }
      }
      
    })
    
    _that.container.addEventListener('click', function (e) {
      let findIndex = null
      
      for (let i = 0; i < _that.dots.length; i++) {
        let x = _that.canvas.width / 2 + _that.dots[i].x - e.layerX
        let y = _that.canvas.height / 2 + _that.dots[i].y - e.layerY
        if (x * x + y * y <= _that.dotRadius*_that.dotRadius) {
          findIndex = i;
          if ( findIndex === 0) {
            findIndex = _that.dots.length;
          }
          _that.activeDotNumber = findIndex;
          _that.flickTrigger = false
          _that.flick.select( _that.activeDotNumber -1, false, false )
          
          _that.render()
          return
        } else {
          findIndex = null
        }
      }
      
    })
  }
  
  
  onResize () {
    let _that = this
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.init()
      // window.location.reload()
    })
  }
}
