export default class FormCtrl {
  constructor (formClass, containerClass) {
    this.form = document.querySelector(formClass);
    this.inputs = [...this.form.querySelectorAll('input')];
    this.labels = [...this.form.querySelectorAll('label')];
    this.container = document.querySelector(containerClass);
    
    this.initLabelUp();
    this.initClickEvent();
  }
  
  initLabelUp () {
    
    let _that = this;
    
    for (let i = 0; i < _that.inputs.length; i++) {
      _that.inputs[i].addEventListener('focus', function () {
        let index = _that.inputs.indexOf(this);
        _that.labels[index].classList.add('active');
      })
      _that.inputs[i].addEventListener('blur', function () {
        let index = _that.inputs.indexOf(this);
        if ( this.value == '' ) _that.labels[index].classList.remove('active');
        
      })
    }
  }
  
  initClickEvent() {
    this.container.addEventListener('click', function() {
      this.classList.add('is-active');
    })
  }
  
}
