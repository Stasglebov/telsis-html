import { TweenLite } from 'gsap';

export default class FormShapes {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      blocksColor: '#fff',
      backgroundColor: 'rgba(0,0,0,0)',
      isAnimated: true
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    
    this.rectWidth = {
      value: 30
    }
    this.rectHeight = {
      value: 55
    }
    this.rectsQuantity = 6
    this.skewX = {
      value: 10
    }
    this.skewY = {
      value: 35
    }
    this.startAlpha = {
      value: 1
    }
    this.alphaAnimation = {
      value: 1
    }
    // this.tlStart = new TimelineMax();
    let _that = this;
    this.tlSec = new TimelineMax();
    this.tl = new TimelineMax({
      repeat: 3,
      yoyo: true,
      onComplete: function() {
        _that.drawOneShape();
      }})
    this.rectsOffset = {
      value: 15
    }
    this.fillColorStyle = '#fff'
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    
    this.render();
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  drawOneShape() {
    let _that = this;
    _that.tlSec
      .to(_that.rectsOffset, 1, {
        value: 35,
        ease: Back.easeOut.config(1.7),
        onUpdate: function () {
          _that.drawRects();
        }
      })
    
  }
  render () {
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.save()
    _that.ctx.fillRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    _that.ctx.restore()
  
    if ( window.innerWidth > 1200 || window.innerWidth < 767 ) {
  
      _that.tl
        .to(_that.rectsOffset, 1, {
          value: 35,
          ease: Back.easeOut.config(1.7),
          onUpdate: function () {
            _that.drawRects();
          }
        })
      // .to(_that.rectsOffset, 1, {
      //   value: 35,
      //   ease: Power3.easeInOut,
      //   onUpdate: function() {
      //     _that.drawRects();
      //   }
      // }, '-=0.8')
  
    } else {
      _that.rectsOffset.value = 35;
    }
    
    _that.drawRects()
    
  }
  
  drawRects () {
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    _that.ctx.fillStyle = _that.backgroundColor;
    for (let i = 0; i < _that.rectsQuantity; i++) {
      _that.ctx.save()
      _that.ctx.beginPath();
      _that.ctx.globalAlpha = 1 - ( 1 / _that.rectsQuantity )*i;
  
      // _that.ctx.globalAlpha = 0.5
      _that.ctx.fillStyle = _that.blocksColor;
      _that.ctx.moveTo( _that.skewX.value + _that.rectsOffset.value*i, 0);
      _that.ctx.lineTo( _that.rectWidth.value + _that.rectsOffset.value*i, _that.skewY.value);
      _that.ctx.lineTo( _that.rectWidth.value - _that.skewX.value + _that.rectsOffset.value*i, _that.rectHeight.value);
      _that.ctx.lineTo(_that.rectsOffset.value*i, _that.rectHeight.value - _that.skewY.value);
      _that.ctx.closePath();
      _that.ctx.fill();
      _that.ctx.restore();
    }
    
  }
  
  onResize () {
    let _that = this;
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.dotsCounter = true
      _that.init()
      // window.location.reload()
    })
  }
}
