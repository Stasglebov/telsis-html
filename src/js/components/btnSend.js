import { TweenLite } from 'gsap';
import Flickity  from 'flickity';


export default class BtnSend {
  
  constructor (containerClass, options) {
    
    // check object assign support
    if (typeof Object.assign != 'function') {
      Object.defineProperty(Object, 'assign', {
        value: function assign (target, varArgs) {
          'use strict'
          if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object')
          }
          
          let to = Object(target)
          
          for (let index = 1; index < arguments.length; index++) {
            let nextSource = arguments[index]
            
            if (nextSource != null) {
              for (let nextKey in nextSource) {
                if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                  to[nextKey] = nextSource[nextKey]
                }
              }
            }
          }
          return to
        },
        writable: true,
        configurable: true
      })
    }
    
    // set default options
    const defaults = {
      color: '#fff',
      fontColor: '#e52165',
      text: 'send',
      blocksNumber: 4,
      shapeWidth: 210,
      shapeHeight: 60,
      blockOffset: {
        value: 4
      },
      direction: 'left',
      offset: {
        value: 0
      }
    }
    const populated = Object.assign(defaults, options)
    for (const key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key]
      }
    }
    
    this.class = containerClass;
    this.tl = new TimelineMax({paused:true});
    this.container = document.querySelector(containerClass)
    this.canvas = {}
    this.canvas.elem = document.createElement('canvas')
    
    this.onResize()
    this.init()
  }
  
  init () {
    // if ( document.querySelector('.slider-clients')) {
    //   this.flickty = new Flickity('.slider-clients', {
    //     pageDots: false,
    //     accessibility: false,
    //     cellAlign: 'left',
    //     initialIndex: 0,
    //     groupCells: 4
    //   })
    // }

    
    window.devicePixelRatio > 1 ? this.initRetinaDisplay() : this.initDefaultDisplay();
    
    this.render()
    this.initHoverEvent()
    // if ( document.querySelector('.slider-clients')) {
    //   this.initFlickChangeEvent()
    // }
  }
  
  initRetinaDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width * 2
    this.canvas.elem.height = this.canvas.height * 2
    this.canvas.elem.style.width = `${this.container.offsetWidth}px`
    this.canvas.elem.style.height = `${this.container.offsetHeight}px`
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
    this.ctx.scale(2, 2)
  }
  
  initDefaultDisplay () {
    this.canvas.width = this.container.offsetWidth
    this.canvas.height = this.container.offsetHeight
    this.canvas.elem.width = this.canvas.width
    this.canvas.elem.height = this.canvas.height
    this.container.appendChild(this.canvas.elem)
    this.ctx = this.canvas.elem.getContext('2d')
  }
  
  render () {
    
    let _that = this
    requestAnimationFrame(function () {
      _that.drawRects
    })
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height)
    
    _that.tl
      .fromTo(_that.blockOffset, 0.4, {
        value: 4
      }, {
        value: 0,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      })
      .fromTo(_that.offset, 0.4, {
        value: 0
      }, {
        value: 12,
        ease: Power2.easeInOut,
        onUpdate: function () {
          _that.drawRects()
        }
      }, '-=0.4')
    // _that.drawRects()
  }
  
  drawRects () {
    
    let _that = this
    _that.ctx.clearRect(0, 0, _that.canvas.width, _that.canvas.height);
    
    // draw text degrees
    for (let i = 0; i < _that.blocksNumber; i++) {
      _that.ctx.save()
      _that.ctx.beginPath();
      _that.ctx.globalAlpha = 1 - ( 1 / _that.blocksNumber )*i;
      
      _that.ctx.strokeStyle = _that.color;
      _that.ctx.fillStyle = _that.color;
      
      _that.ctx.moveTo( (_that.blocksNumber - 1)*_that.blockOffset.value - i*_that.blockOffset.value + _that.offset.value, i*_that.blockOffset.value + 0.5);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value + _that.shapeWidth - i*_that.blockOffset.value - 35 + _that.offset.value, i*_that.blockOffset.value + 0.5);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value + _that.shapeWidth - i*_that.blockOffset.value + _that.offset.value, _that.shapeHeight + i*_that.blockOffset.value + 0.5);
      _that.ctx.lineTo( (_that.blocksNumber - 1)*_that.blockOffset.value - i*_that.blockOffset.value + 35 + _that.offset.value, _that.shapeHeight + i*_that.blockOffset.value + 0.5);
      _that.ctx.closePath();
      _that.ctx.fill();
      _that.ctx.restore();
    }
    
    // draw text
    
    _that.ctx.save();
    _that.ctx.beginPath();
    _that.ctx.fillStyle = _that.fontColor;
    _that.ctx.textAlign = 'center';
    _that.ctx.textBaseline = 'middle';
    _that.ctx.font = "16px ApercuBold";
    _that.ctx.fillText(`${_that.text}`, _that.canvas.width / 2, _that.canvas.height / 2 - 10);
    _that.ctx.closePath();
    _that.ctx.restore();
    
    
    
  }
  
  
  // initFlickChangeEvent() {
  //   this.flickty.on( 'change', ( index ) => {
  //     this.tl.time(0)
  //     this.drawRects()
  //   });
  // }
  
  initHoverEvent() {
    let _that = this;
    let access = true
    _that.container.addEventListener('mouseenter', function() {
      this.style.cursor = 'pointer';
      access = false;
      _that.tl.play();
    })
    _that.container.addEventListener('mouseleave', function() {
      if ( access ) return;
      this.style.cursor = 'default';
      access = true;
      _that.tl.reverse();
    })
  }
  onResize () {
    let _that = this
    // change canvas size and reinit()
    if (window.innerWidth < 1020) return
    window.addEventListener('resize', () => {
      _that.canvas.width = _that.container.offsetWidth
      _that.canvas.height = _that.container.offsetHeight
      _that.canvas.elem.width = _that.canvas.width
      _that.canvas.elem.height = _that.canvas.height
      _that.init()
      window.location.reload()
    })
  }
}
