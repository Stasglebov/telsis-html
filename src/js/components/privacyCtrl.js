class PrivacyCtrl {
  constructor () {
    this.acceptBtn = document.querySelector('.accept-btn');
    this.closeBtn = document.querySelector('.privacy .close-btn');
    this.container = document.querySelector('.privacy');
    this.init();
  }
  init() {
    this.acceptBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.container.classList.add('is-close');
    })
    this.closeBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.container.classList.add('is-close');
    })
  }
}
export const privacyCtrl = new PrivacyCtrl();
