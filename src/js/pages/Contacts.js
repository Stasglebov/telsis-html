/**
 * Home page scripts.
 *
 * @module Contacts
 */
import { Resp } from '../modules/dev/_helpers';
import Preloader from '../components/Preloader';

export default class Contacts {

  constructor() {
    // initialize after construction
    this.init();
  }

  /**
   * Example method.
   */
  example() {
    console.log(this.message);
  };

  /**
   * Initialize Home page scripts.
   */
  init() {
    this.example();
  }
}
