'use strict'

/**
 * App entry point.
 *
 * @module App
 */

/** Import page controllers */
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'
import './components/polyfills'
import 'core-js/shim'
import './components/changePageElementsDesign'
import Preloader from './components/Preloader'
import Shapes from './components/Shapes'
import Slider from './components/Slider'
import SolutionsSlider from './components/solutionSlider'
import Btn from './components/Button'
import Menu from './components/Menu'
import GradientBackground from './components/gradientBackground'
import BtnType2 from './components/ButtonType2'
import Figure1 from './components/figure1'
import Figure2 from './components/figure2'
import Figure3 from './components/figure3'
import BtnSend from './components/btnSend'
import FormShapes from './components/formShapes'
import ScrollHeaderToggle from './components/headerCtrl'
import Services from './components/servicesCtrl'
import Faded from './components/fading'
import FormCtrl from './components/formCtrl'
import Dropdown from './components/Dropdown'
import AboutShapes from './components/about-shapes'
import './components/parallaxAbout'
import './components/form'

import { privacyCtrl } from './components/privacyCtrl'

import './components/scrollDownCtrl'

// ScrollHeaderToggle.initThrottle()
Faded.init()

new BtnSend('.accept-btn', {
  color: '#fff',
  text: 'accept',
  fontColor: '#ea2066'
})

import { currentPage } from './modules/dev/_helpers'

/**
 * Run appropriate scripts for each page.
 *
 **/
switch (currentPage) {
  /** Home page */
  case 'home': {
    
    new Preloader('.preloader')
    new Shapes('.left-wrapper', {
      blocksColor: '#eee'
    })
    
    new FormCtrl('.form', '.talk')
    new Services()
    
    new Slider('.graph-wrapper', {
      dotsQuantity: document.querySelectorAll('.slider-item').length
    })
    new Btn('.slider-thumb .flickity-prev-next-button.previous', {
      direction: 'left'
    })
    new Btn('.slider-thumb .flickity-prev-next-button.next', {
      direction: 'right'
    })
    new Btn('.slider-clients .flickity-prev-next-button.previous', {
      direction: 'left'
    })
    new Btn('.slider-clients .flickity-prev-next-button.next', {
      direction: 'right'
    })
    new BtnSend('.btn-send')
    new BtnSend('.more-btn-1', {
      color: '#ea2066',
      text: 'find out more',
      fontColor: '#fff'
    })
    new BtnSend('.more-btn-2', {
      color: '#ea2066',
      text: 'find out more',
      fontColor: '#fff'
    })
    new FormShapes('.form-shapes')
    
    new BtnType2('.js-operators-link')
    new BtnType2('.js-enterprise-link')
    
    new Menu('.icon-menu-wrapper', {
      parentContainer: '.burger'
    })
    new Figure1('.figure1')
    new Figure2('.figure2')
    new Figure3('.figure3')
    new GradientBackground('.backgroundCanvas')
    break
  }
  /** Contacts page */
  case 'contacts': {
    new Menu('.icon-menu-wrapper', {
      parentContainer: '.burger'
    })
    new FormCtrl('.form', '.form_contacts')
    
    new BtnSend('.form_contacts .right .form-shapes', {
      color: '#ea2066',
      text: 'send',
      fontColor: '#fff',
      shapeWidth: 255
    })
    new Shapes('.intro_contacts__canvas', {
      blocksColor: '#eee'
    })
    // new Dropdown();
    break
  }
  case 'solutions': {
    new Menu('.icon-menu-wrapper', {
      parentContainer: '.burger'
    })
    new SolutionsSlider('.solutions-slider__inner .center', {
      containerLeftClass: '.operators-list',
      containerRightClass: '.contacts-list'
    })
  
    break;
  }
  case 'about': {
    // new ScrollCtrl('.scroll-up', '.intro_about');
    new Menu('.icon-menu-wrapper', {
      parentContainer: '.burger'
    })
    new FormShapes('.form-shapes')
    new FormCtrl('.form', '.talk')
    new BtnSend('.btn-send')
    if (window.innerWidth > 1199) {
      new AboutShapes('.canvas-wrapper-about')
    }
    break;
  }
  
  case 'product': {
    new BtnSend('.brochure-btn', {
      text: 'get a brochure'
    })
    new BtnSend('.get-brochure-btn', {
      text: 'get a brochure',
      shapeWidth: 255
    })
    new BtnSend('.form_contacts .right .form-shapes', {
      color: '#ea2066',
      text: 'send',
      fontColor: '#fff',
      shapeWidth: 255
    })
    new Shapes('.canvas-wrapper-product', {
      blocksColor: '#eee'
    })
    new FormCtrl('.form', '.form_contacts')
    new Menu('.icon-menu-wrapper', {
      parentContainer: '.burger'
    })
    break;
  }
  
  /** No page found */
  default:
    break
}
