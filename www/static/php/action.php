<?php
require 'form.php';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST;
    if (!empty($data['email'])) {
        $submit = new Submit();
        $submit->save($data);
        $submit->send($data);
        $submit->sendAdmin($data);
    }
}
?>