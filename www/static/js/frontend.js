// serialize
window.serialize = function (form) {
    var formel = document.querySelector(form),
      inputs = formel.querySelectorAll("input, select, textarea"),
      obj = {}, key;
    for (key in inputs) {
      if (inputs[key].tagName) {
        if (inputs[key].type === "checkbox") {
          obj[inputs[key].name] = inputs[key].checked === true ? inputs[key].value : false;
        } else {
          obj[inputs[key].name] = inputs[key].value;
        }
      }
    }
    return obj;
  }
window.toString = function param(object) {
  var encodedString = '';
  for (var prop in object) {
    if (object.hasOwnProperty(prop)) {
      if (encodedString.length > 0) {
        encodedString += '&';
      }
      encodedString += encodeURI(prop + '=' + object[prop]);
    }
  }
  return encodedString;
}
window.addEventListener('load', function(){
  if (localStorage.getItem("cookies")) {
    document.querySelector('.privacy').remove();
  }
});

if ( document.querySelector('.form') ) {
  document.querySelector('.form').addEventListener('submit', function (e) {
    e.preventDefault();
    var href = window.location.href;
    // var form = document.querySelector('.form');
    var formData = toString(serialize('.form'))
    var data = formData + "&href=" + href;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', './static/php/action.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(data);
    document.querySelector('.form').reset();
  });
}



document.querySelector('.accept-btn').addEventListener('click', function(e){
  e.preventDefault();
  if (localStorage.getItem("cookies") === null) {
    localStorage.setItem("cookies", true);
  }
})